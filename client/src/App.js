import React, { Component } from 'react';
import './App.css';

class App extends Component {
  render() {
      const { children } = this.props;
      return <div>
          <div>{children}</div>
      </div>
  }
}

export default App;
