import React, { Component } from 'react';
import { setMessage, cleanMessage} from '../../reducer/Actions';
import { connect } from 'react-redux';
import './SendMessage.css';
class SendMessage extends Component {
    sendMessage = () => {
        var time = new Date();
        var key = time.valueOf();
        var isDeleted = false;
        const { hubConnection, user, message, cleanMessage} = this.props;
        hubConnection
            .invoke('SendMessage', user, message, time, key, isDeleted)
            .catch(err => console.error(err));
        cleanMessage();
    };
    render() {
        const { setMessage , message } = this.props;
        return (
            <div className="row">
                <div className="col-8">
                    <input
                        type="text"
                        value={message}
                        className="form-control"
                        onChange={e => setMessage(e.target.value)} />
                </div>
                <div className="col-4">
                    <button onClick={this.sendMessage} disabled={!message} className="btn btn-primary btn-block">Send</button>
                </div>
                
                
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        user: state.chat.user,
        message: state.chat.newMessage
    };
};
;

const mapDispatchToProps = {
    setMessage,
    cleanMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(SendMessage);