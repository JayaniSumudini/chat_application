import React, { Component } from 'react';
import { connect } from 'react-redux';
import {cleanMessage } from '../../reducer/Actions';
import './IncomeMessage.css';

class IncomeMessage extends Component {
    deleteMessage = () => {
        var isDeleted = true;
        const { hubConnection, user, messageItem, cleanMessage } = this.props;
        hubConnection
            .invoke('SendMessage', user, messageItem.message, messageItem.time, messageItem.key, isDeleted)
            .catch(err => console.error(err));
        cleanMessage();
    };
    render() {
        const { messageItem, user } = this.props;
        const cssClassName = (JSON.stringify(messageItem.user) === JSON.stringify(user)) ? "messagefrom" : "message";
        return (
            <div className={cssClassName}>
                <div>
                    <span className="user"> {messageItem.user} </span>
                    <span className="time"> {new Date(messageItem.time).toLocaleString()} </span>
                    {messageItem.user === user &&
                        <button onClick={this.deleteMessage} className="deleteBtn">X</button>
                    }
                </div>
                <div>
                    <span style={{ display: 'block' }} className="msg"> {(!messageItem.isDeleted) ? messageItem.message : 'message deleted'}  </span>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {};
};
;

const mapDispatchToProps = {
    cleanMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(IncomeMessage);