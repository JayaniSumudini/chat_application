import React, { Component } from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import IncomeMessage from '../../components/incomeMessage/IncomeMessage';
import SendMessage from '../../components/sendMessage/SendMessage';
import { addMessage } from '../../reducer/Actions';
import './Chat.css';

const signalR = require("@aspnet/signalr");

class Chat extends Component {
    constructor(props) {
        super(props);

        this.state = {
            hubConnection: null
        };
    }

    componentDidMount = () => {
        const { user, addMessage } = this.props;
        if (user === "") {
            browserHistory.push('/')
        }

        const hubConnection = new signalR.HubConnectionBuilder()
            .withUrl("http://localhost:5000/chatroom")
            .build();

        this.setState({ hubConnection, user }, () => {
            this.state.hubConnection
                .start()
                .then(() => console.log('Connection started!'))
                .catch(err => console.log('Error while establishing connection :('));

            this.state.hubConnection.on('ReceiveMessage', (user, receivedMessage, time, key, isDeleted) => {
                const message = {
                    user: user,
                    message: receivedMessage,
                    time: time,
                    key: key,
                    isDeleted: isDeleted
                }
                addMessage(message)
            });
        });
    };

    

    render() {
        const { messages, user } = this.props;
        const cssClassName = (messages.length > 0) ? "chatbox" : "";

        return (
            <div>
                <br />
                <SendMessage hubConnection={this.state.hubConnection}/>
                <div className={cssClassName}>
                    {messages.map((messageItem, index) => (
                        <div key={index}>
                            <IncomeMessage messageItem={messageItem} user={user} hubConnection={this.state.hubConnection}/>
                        </div>
                    ))}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        user: state.chat.user,
        messages: state.chat.messages,
        message: state.chat.newMessage
    };
};
;

const mapDispatchToProps = {
    addMessage,
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);