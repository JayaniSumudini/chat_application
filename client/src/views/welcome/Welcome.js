import React, { Component } from 'react';
import { browserHistory } from 'react-router'

import { connect } from 'react-redux';
import { addUser } from '../../reducer/Actions'
import { LOGONAME } from '../../imges'

class Welcome extends Component {
    handleSubmit(user) {
        this.props.addUser(user);
    }

    render() {
        return (
            <div className="form-group text-center">
                <img src={LOGONAME}></img>
                <input className="form-control"
                    type="text"
                    onChange={e => this.handleSubmit(e.target.value)} />
                <button onClick={() => browserHistory.push('/chatroom')} className="btn btn-primary btn-block">GO LIVE</button>
            </div>
        );
    }
}


const mapStateToProps = state => {
    return {};
};
;

const mapDispatchToProps = {
    addUser
};

export default connect(mapStateToProps,mapDispatchToProps)(Welcome);
