import { ADD_MESSAGE } from "./Types";
import { ADD_USER } from "./Types";
import { SET_MESSAGE } from "./Types";
import { CLEAN_MESSAGE } from "./Types";

const init_state = {
    user: "",
    newMessage: "",
    messages: []
}

const setAddOrDeleteMessage = (state, messageItem) => {
    if (messageItem.isDeleted) {
        var newMessages = [];
        state.messages.map((stateMessage) => {
            if (stateMessage.key === messageItem.key) {
                newMessages.push(messageItem);
            } else {
                newMessages.push(stateMessage);
            }
        });
        return {
            ...state,
            messages: newMessages
        }
    } else {
        return {
            ...state,
            messages: state.messages.concat([messageItem])
        }
    }
} 

export default function reducer(state = init_state, action) {
    switch (action.type) {
        case ADD_MESSAGE:
            return setAddOrDeleteMessage(state, action.payload.message)
            
        case ADD_USER:
            return {
                ...state,
                user: action.payload.user
            }
        case SET_MESSAGE:
            return {
                ...state,
                newMessage: action.payload.message
            }
        case CLEAN_MESSAGE:
            return {
                ...state,
                newMessage: ""
            }
        default:
            return state;
    }
}
