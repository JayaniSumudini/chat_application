import { ADD_MESSAGE } from "./Types";
import { ADD_USER } from "./Types";
import { SET_MESSAGE } from "./Types";
import { CLEAN_MESSAGE } from "./Types";

export function addMessage(message) {
    return {
        type: ADD_MESSAGE,
        payload: {
            message: message
        }
    }
}

export function addUser(user) {
    return {
        type: ADD_USER,
        payload: {
            user: user
        }
    }
}

export function setMessage(message) {
    return {
        type: SET_MESSAGE,
        payload: {
            message: message
        }
    }
}

export function cleanMessage() {
    return {
        type: CLEAN_MESSAGE
    }
}