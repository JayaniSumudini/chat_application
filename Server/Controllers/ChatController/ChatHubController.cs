﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace Server.Controllers.ChatController
{
    public class ChatHubController : Hub
    {
        public async Task SendMessage(string user, string message, DateTime time, string key, bool isDeleted)
        {
            await Clients.All.SendAsync("ReceiveMessage", user, message, time, key, isDeleted);
        }
    }
}
