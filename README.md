# ChatMore

ChatMore is a chat application that allows users to send messages from different browser
sessions. A user should also be able to delete a message which has been sent. Each message
should display the user’s initials and a timestamp of when the message was sent

### Tech

ChatMore uses a number of frameworks and libraries to work properly:

* [React] - A JavaScript library for building user interfaces.
* [Redux] - predictable state container for JavaScript apps.
* [DotNet core] - open source developer cross-platform for building many different types of applications.

And of course ChatMore itself is open source with a [public repository][dill]
 on Bitbucket.

### Installation

ChatMore requires [Node.js](https://nodejs.org/) to run.

Install the dependencies and start the client server.

```sh
$ cd client
$ npm install
$ npm start
```

For start server...

```sh
$ cd Server
$ dotnet run
```


License
----

MIT


**Free Software, !**

Developer : Jayani Sumudini
[Linkedin] , [web], [Github], [stackoverflow]


   [dill]: <https://bitbucket.org/JayaniSumudini/chat_application/src/master/>
   [React]: <https://reactjs.org/>
   [DotNet core]: <https://www.microsoft.com/net>
   [Redux]: <https://redux.js.org/>
   [Linkedin]: <https://www.linkedin.com/in/jayanisumudini>
   [web]: <http://jayani.eigenware.com/>
   [Github]: <https://github.com/JayaniSumudini>
   [stackoverflow]: <https://stackoverflow.com/users/5355145/jayani-sumudini?tab=profile>
  
